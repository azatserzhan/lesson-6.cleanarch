package kz.azatserzhanov.currency.contract

import kz.azatserzhanov.common.base.MvpPresenter
import kz.azatserzhanov.common.base.MvpView
import kz.azatserzhanov.currency.model.CurrencyItem

interface MainContract {

    interface View : MvpView {
        fun showResultCurrency(total: String)
        fun showCurrentCurrency(total: String)
        fun showResultButton(isVisible: Boolean)
        fun showCurrencyList(list: List<CurrencyItem>)
        fun showResultValueText(text: String)
        fun showSozdik(text: String)
    }

    interface Presenter : MvpPresenter<View> {
        fun loadCurrency()
        fun setCurrencyResult(inputValue: Double)
        fun currentCurrencyChange(inputValue: Double)
        fun resultCurrencyChange(inputValue: Double)
        fun chooseCurrency(isCurrent: Boolean, inputValue: Double)
        fun setCurrencyList()
        fun changResultCurrencyValue(position: Int)
        fun loadData()
    }
}
package kz.azatserzhanov.currency

import kz.azatserzhanov.common.base.InjectionModule
import kz.azatserzhanov.currency.api.ExchangeApiService
import kz.azatserzhanov.currency.interactor.CurrencyInteractor
import kz.azatserzhanov.currency.presenter.MainPresenter
import org.koin.dsl.module

object ExchangeModule : InjectionModule {
    override fun create() = module {
        single { MainPresenter(get()) }
        single { CurrencyInteractor(get()) }
        single { ExchangeApiService.create() }
    }
}
package kz.azatserzhanov.currency.interactor

import io.reactivex.Single
import kz.azatserzhanov.currency.api.ExchangeApiService
import kz.azatserzhanov.currency.model.Currency

class CurrencyInteractor(private val apiService: ExchangeApiService) {
    fun getCurrency(): Single<Currency> = apiService.get()
}
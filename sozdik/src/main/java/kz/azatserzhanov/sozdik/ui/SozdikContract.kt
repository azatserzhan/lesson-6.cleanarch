package kz.azatserzhanov.sozdik.ui

import kz.azatserzhanov.common.base.MvpPresenter
import kz.azatserzhanov.common.base.MvpView
import kz.azatserzhanov.sozdik.model.SozdikItem

interface SozdikContract {

    interface View : MvpView {
        fun show(list: List<SozdikItem>)
        fun hide()
    }

    interface Presenter : MvpPresenter<View> {
        fun load()
        fun save(title: String, description: String)
        fun removeItem(postion: Int)
    }
}
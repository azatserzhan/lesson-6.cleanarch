package kz.azatserzhanov.sozdik.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import kotlinx.android.synthetic.main.fragment_sozdik.*
import kz.azatserzhanov.common.base.BaseFragment
import kz.azatserzhanov.sozdik.R
import kz.azatserzhanov.sozdik.model.SozdikItem
import org.koin.androidx.viewmodel.ext.android.viewModel

class SozdikFragment(val text: String) : BaseFragment<SozdikContract.View, SozdikContract.Presenter>(),
    SozdikContract.View {

    companion object {
        fun create(text: String) = SozdikFragment(text)
    }

    private val presenterImpl: SozdikPresenter by viewModel()
    override val presenter: SozdikContract.Presenter
        get() = presenterImpl
    private var sozdikAdapter: SozdikAdapter? = null


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View =
        inflater.inflate(R.layout.fragment_sozdik, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        button.setOnClickListener {
            presenter.save(
                titleEditText.text.toString(),
                descriptionEditText.text.toString()
            )
            titleEditText.setText("")
            presenter.load()
        }

        presenter.load()

        sozdikAdapter = SozdikAdapter(
            clickListener = {
                presenter.removeItem(it)
            }
        )

        val currencyManager = LinearLayoutManager(
            context,
            LinearLayoutManager.VERTICAL,
            false
        )

        sozdikRecyclerView.apply {
            layoutManager = currencyManager
            adapter = sozdikAdapter
        }

        button.text = text

        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)
    }

    override fun show(list: List<SozdikItem>) {
        sozdikAdapter?.addItems(list)
    }

    override fun hide() {
        TODO("Not yet implemented")
    }
}
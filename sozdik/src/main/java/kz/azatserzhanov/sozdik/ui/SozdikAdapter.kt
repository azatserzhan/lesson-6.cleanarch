package kz.azatserzhanov.sozdik.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_sozdik.view.*
import kz.azatserzhanov.sozdik.R
import kz.azatserzhanov.sozdik.model.SozdikItem

class SozdikAdapter(
    private val clickListener: (position: Int) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val sozdikList = mutableListOf<SozdikItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return CurrencyViewHolder(inflater, parent)
    }

    override fun getItemCount(): Int = sozdikList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as CurrencyViewHolder).bind(sozdikList[position], clickListener)
    }

    fun addItems(list: List<SozdikItem>) {
        sozdikList.clear()
        sozdikList.addAll(list)
        notifyDataSetChanged()
    }

    private class CurrencyViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.item_sozdik, parent, false)) {
        private val titleTextView = itemView.titleTextView
        private val descriptionTextView = itemView.descriptionTextView

        fun bind(item: SozdikItem, clickListener: (position: Int) -> Unit) {
            titleTextView.text = item.title
            descriptionTextView.text = item.description
            titleTextView.setOnClickListener {
                clickListener(adapterPosition)
            }
        }
    }
}
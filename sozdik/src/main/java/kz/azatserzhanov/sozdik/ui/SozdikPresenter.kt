package kz.azatserzhanov.sozdik.ui

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kz.azatserzhanov.common.base.BasePresenter
import kz.azatserzhanov.sozdik.api.SozdikApi
import kz.azatserzhanov.sozdik.interactor.SozdikInteractor
import kz.azatserzhanov.sozdik.model.SozdikItem
import timber.log.Timber

class SozdikPresenter(
    private val sozdikApi: SozdikApi,
    private val sozdikInteractor: SozdikInteractor
) :
    BasePresenter<SozdikContract.View>(),
    SozdikContract.Presenter {

    private var sozdikList = mutableListOf<SozdikItem>()

    override fun load() {
        sozdikInteractor.getSozdik()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { list ->
                    view?.show(list)
                    sozdikList.clear()
                    sozdikList.addAll(list)
                },
                {
                    Timber.e(it, "Error to load")
                }
            )
            .disposeOnCleared()
    }

    override fun save(title: String, description: String) {
        if (title.isNotEmpty()) {
            sozdikList.add(SozdikItem(title, description))
            sozdikApi.saveSozdik(sozdikList)
        }
    }

    override fun removeItem(postion: Int) {
        sozdikList.removeAt(postion)
        sozdikApi.saveSozdik(sozdikList)
        view?.show(sozdikList)
    }
}

package kz.azatserzhanov.sozdik

import android.preference.PreferenceManager
import kz.azatserzhanov.common.base.InjectionModule
import kz.azatserzhanov.sozdik.api.SozdikApi
import kz.azatserzhanov.sozdik.interactor.SozdikInteractor
import kz.azatserzhanov.sozdik.ui.SozdikPresenter
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object SozdikModule : InjectionModule {
    override fun create() = module {
        single { SozdikApi(PreferenceManager.getDefaultSharedPreferences(get())) }
        single { SozdikInteractor(get()) }
        viewModel { SozdikPresenter(get(), get()) }
    }
}
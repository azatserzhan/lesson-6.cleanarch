package kz.azatserzhanov.sozdik.api

import android.content.SharedPreferences
import io.reactivex.Single
import kz.azatserzhanov.sozdik.model.SozdikItem

private const val TITLE_PREF = "TITLE_PREF"
private const val DESCRIPTIONS_PREF = "DESCRIPTIONS_PREF"

class SozdikApi(
    private val prefs: SharedPreferences
) {
    fun saveTitle(titles: Set<String>) {
        val editor = prefs.edit()
        editor?.putStringSet(TITLE_PREF, titles)
        editor?.apply()
    }

    fun saveDescription(descriptions: Set<String>) {
        val editor = prefs.edit()
        editor?.putStringSet(DESCRIPTIONS_PREF, descriptions)
        editor?.apply()
    }

    fun saveSozdik(sozdikList: List<SozdikItem>) {
        val editor = prefs.edit()
        val titles = sozdikList.map { it.title }.toSet()
        val descriptions = sozdikList.map { it.description }.toSet()

        editor?.putStringSet(TITLE_PREF, titles)
        editor?.putStringSet(DESCRIPTIONS_PREF, descriptions)

        editor?.apply()
    }

    fun getTitles(): Single<List<String>> =
        Single.fromCallable {
            prefs.getStringSet(TITLE_PREF, mutableSetOf(""))?.toList()
        }

    fun getDescriptions(): Single<List<String>> =
        Single.fromCallable {
            prefs.getStringSet(DESCRIPTIONS_PREF, mutableSetOf(""))?.toList()
        }
}
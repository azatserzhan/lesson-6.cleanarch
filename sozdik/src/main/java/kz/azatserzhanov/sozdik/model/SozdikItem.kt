package kz.azatserzhanov.sozdik.model

data class SozdikItem(
    val title: String,
    val description: String
)
package kz.azatserzhanov.sozdik.interactor

import io.reactivex.Single
import io.reactivex.functions.BiFunction
import kz.azatserzhanov.sozdik.api.SozdikApi
import kz.azatserzhanov.sozdik.model.SozdikItem

class SozdikInteractor(
    private val sozdikApi: SozdikApi
) {
    fun getSozdik(): Single<List<SozdikItem>> = Single.zip(
        sozdikApi.getTitles(),
        sozdikApi.getDescriptions(),
        BiFunction { titles, descriptions ->
            val result = mutableListOf<SozdikItem>()
            titles.forEachIndexed { index, title ->
                descriptions.let {
                    result.add(SozdikItem(title, it[index]))
                }
            }
            result
        })
}
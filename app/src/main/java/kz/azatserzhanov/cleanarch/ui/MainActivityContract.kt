package kz.azatserzhanov.cleanarch.ui

import kz.azatserzhanov.common.base.MvpPresenter
import kz.azatserzhanov.common.base.MvpView

interface MainActivityContract {

    interface View : MvpView {
        fun showSomeView()
    }

    interface Presenter : MvpPresenter<View> {
        fun load()
    }
}
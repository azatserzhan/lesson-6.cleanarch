package kz.azatserzhanov.cleanarch.activity

import android.os.Bundle
import com.google.android.gms.ads.MobileAds
import kz.azatserzhanov.cleanarch.R
import kz.azatserzhanov.cleanarch.ui.MainActivityContract
import kz.azatserzhanov.cleanarch.ui.MainActivityPresenter
import kz.azatserzhanov.common.base.BaseActivity
import kz.azatserzhanov.common.contract.AppRouterContract
import kz.azatserzhanov.currency.ui.CurrencyFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : BaseActivity(), MainActivityContract.View, AppRouterContract {

    private val presenter: MainActivityPresenter by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        MobileAds.initialize(this)
        showCurrency()
    }

    override fun showCurrency() {
        replaceFragment(CurrencyFragment.create())
    }

    override fun showSomeView() {

    }
}

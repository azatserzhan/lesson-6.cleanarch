package kz.azatserzhanov.cleanarch

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

@Suppress("ConstantConditionIf")
class App : Application() {

    // private val analytics: AnalyticsContract by inject()
    // private val preferences: PreferencesContract by inject()

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@App)
            if (BuildConfig.DEBUG) printLogger()
            modules(KoinModules.modules)
        }
    }
}
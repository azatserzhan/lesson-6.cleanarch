package kz.azatserzhanov.cleanarch

import kz.azatserzhanov.currency.ExchangeModule
import kz.azatserzhanov.sozdik.SozdikModule
import org.koin.core.module.Module

object KoinModules {
    val modules: List<Module> =
        listOf(
            ExchangeModule.create(),
            SozdikModule.create()
        )
}